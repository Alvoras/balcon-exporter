package exporters

import (
	"fmt"
	"time"
)

// type Services struct {
// 	List []*Service
// }

type StateExporterConfig struct {
	ServiceNames []string `json:"services"`
	Interval     int      `json:"interval"`
}

type StateExporter struct {
	Services    []*Service
	StatusMap   map[string]int
	GetStatuses func([]*Service) map[string]string
	Interval    int
}

type Service struct {
	Name   string
	Status string
}

func (seConfig *StateExporterConfig) NewStateExporter(statusMap map[string]int, dataFunc func([]*Service) map[string]string, config *StateExporterConfig) *StateExporter {
	se := &StateExporter{
		StatusMap:   statusMap,
		GetStatuses: dataFunc,
		Interval:    seConfig.Interval,
	}

	for _, name := range seConfig.ServiceNames {
		se.Services = append(se.Services, NewService(name))
	}

	return se
}

func NewService(name string) *Service {
	s := &Service{
		Name:   name,
		Status: "UNKNOWN",
	}

	return s
}

func (exp *StateExporter) GetAllStatusCode() map[string]int {
	statuses := map[string]int{}

	for _, service := range exp.Services {
		statuses[service.Name] = exp.GetServiceStatusID(service.Status)
	}

	return statuses
}

func (exp *StateExporter) GetAllStatusString() map[string]string {
	statuses := map[string]string{}

	for _, service := range exp.Services {
		statuses[service.Name] = service.Status
	}

	return statuses
}

func (exp *StateExporter) GetStatus(name string) string {
	service, err := exp.Get(name)
	if err != nil {
		return "N/A"
	}
	return service.Status
}

func (exp *StateExporter) Get(name string) (*Service, error) {
	for _, service := range exp.Services {
		if service.Name == name {
			return service, nil
		}
	}

	return nil, fmt.Errorf("service not found")
}

func (exp *StateExporter) Register(name string) {
	exp.Services = append(exp.Services, NewService(name))
}

func (exp *StateExporter) RegisterAll(names []string) {
	for _, name := range names {
		exp.Services = append(exp.Services, NewService(name))
	}
}

func (exp *StateExporter) UpdateServiceStatus(name string, status string) error {
	s, err := exp.Get(name)
	if err != nil {
		return err
	}

	s.Status = status

	return nil
}

func (exp *StateExporter) Poll() {
	for {
		exp.UpdateAllStatus()
		time.Sleep(time.Duration(exp.Interval) * time.Second)
	}
}

func (exp *StateExporter) UpdateAllStatus() {
	data := exp.GetStatuses(exp.Services)

	for name, status := range data {
		exp.UpdateServiceStatus(name, status)
	}
}

func (exp *StateExporter) GetServiceStatusID(status string) int {
	if _, ok := exp.StatusMap[status]; ok {
		return exp.StatusMap[status]
	}

	return exp.StatusMap["UNKNOWN"]
}
