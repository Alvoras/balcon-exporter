package systemctl

import (
	"os/exec"
	"strings"

	"gitlab.com/Alvoras/balcon-exporter/internal/config"
	"gitlab.com/Alvoras/balcon-exporter/internal/exporters"
	"gitlab.com/Alvoras/balcon-exporter/internal/logger"
)

var (
	Exporter *exporters.StateExporter
)

func GetStatusMap() map[string]int {
	return map[string]int{
		"active":   config.Cfg.GrafanaValues["RUNNING"],
		"inactive": config.Cfg.GrafanaValues["DOWN"],
	}
}

func GetStatuses(services []*exporters.Service) map[string]string {
	data := map[string]string{}

	for _, service := range services {
		raw, err := exec.Command("systemctl", "show", service.Name, "--no-page", "-p", "ActiveState").Output()
		if err != nil {
			logger.Error.Println("Failed to run systemctl for ", service.Name, err)
			return data
		}

		output := strings.TrimRight(string(raw), "\n")
		line := strings.SplitN(output, "=", 2)

		name, status := service.Name, line[1]

		data[name] = status
	}

	return data
}
