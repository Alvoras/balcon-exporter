package logger

import (
	"io"
	"log"
	"os"
	"time"
)

var (
	Info    *log.Logger
	Warning *log.Logger
	Error   *log.Logger
	Success *log.Logger
	HTTP    *log.Logger
)

func init() {
	var infoWriter io.Writer = os.Stdout
	var warningWriter io.Writer = os.Stdout
	var errorWriter io.Writer = os.Stdout
	var successWriter io.Writer = os.Stdout

	Success = log.New(successWriter,
		time.Now().Format("2006-01-02 15:04:05")+" -- [+] ",
		0,
	)

	Info = log.New(infoWriter,
		time.Now().Format("2006-01-02 15:04:05")+" -- [i] ",
		0,
	)

	Warning = log.New(warningWriter,
		time.Now().Format("2006-01-02 15:04:05")+" -- [!] ",
		0,
	)

	Error = log.New(errorWriter,
		time.Now().Format("2006-01-02 15:04:05")+" -- [x] ",
		0,
	)
}
