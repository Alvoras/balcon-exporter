package supervisord

import (
	"os/exec"
	"strings"

	"gitlab.com/Alvoras/balcon-exporter/internal/config"
	"gitlab.com/Alvoras/balcon-exporter/internal/exporters"
	"gitlab.com/Alvoras/balcon-exporter/internal/logger"
)

var (
	Exporter *exporters.StateExporter
)

func GetStatusMap() map[string]int {
	return map[string]int{
		"FATAL":    config.Cfg.GrafanaValues["FATAL"],
		"BACKOFF":  config.Cfg.GrafanaValues["FATAL"],
		"RUNNING":  config.Cfg.GrafanaValues["RUNNING"],
		"STARTING": config.Cfg.GrafanaValues["STARTING"],
		"STOPPED":  config.Cfg.GrafanaValues["DOWN"],
		"UNKNOWN":  config.Cfg.GrafanaValues["UNKOWN"],
	}
}

func GetStatuses(services []*exporters.Service) map[string]string {
	data := map[string]string{}

	raw, err := exec.Command("supervisorctl", "status", "all").Output()
	if err != nil {
		logger.Error.Println("Failed to run supervisorctl", err)

		return data
	}

	output := strings.TrimRight(string(raw), "\n")

	linesplit := strings.Split(output, "\n")
	// linesplit = linesplit[:len(linesplit)-1] // Remove last empty item

	for _, line := range linesplit {
		line := strings.Fields(line)
		name, status := line[0], line[1]

		data[name] = status
	}

	return data
}
