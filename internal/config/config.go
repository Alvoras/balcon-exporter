package config

import (
	"encoding/json"
	"io/ioutil"
	"os"

	"gitlab.com/Alvoras/balcon-exporter/internal/exporters"
	"gitlab.com/Alvoras/balcon-exporter/internal/logger"
)

var (
	Cfg = new(Config)
)

// type Exporter struct {
// 	Services []string `json:"services"`
// }

type Exporters struct {
	Supervisord *exporters.StateExporterConfig `json:"supervisord"`
	Systemctl   *exporters.StateExporterConfig `json:"systemctl"`
}

// Config structure which mirrors the json file
type Config struct {
	Port          string         `json:"port"`
	Exporters     *Exporters     `json:"exporters"`
	GrafanaValues map[string]int `json:"grafana-values"`
}

func (cfg *Config) Load() {
	filepath := "config.json"
	cfgData, err := ioutil.ReadFile(filepath)
	if err != nil {
		logger.Error.Println("Failed to load config file", err)
	}

	if err := json.Unmarshal(cfgData, &cfg); err != nil {
		logger.Error.Printf("Failed to load the config file (%s)\n", filepath)
		logger.Error.Println(err)
		os.Exit(1)
	}
}
