package router

import "github.com/valyala/fasthttprouter"

var (
	R *fasthttprouter.Router
)

func init() {
	R = fasthttprouter.New()
	R.GET("/", Ok)
	R.GET("/query/:name", StatusByName)
	R.GET("/query", Status)
}
