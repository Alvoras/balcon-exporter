package router

import (
	"encoding/json"
	"fmt"
	"strconv"

	"github.com/valyala/fasthttp"
	"github.com/valyala/fasthttprouter"
	"gitlab.com/Alvoras/balcon-exporter/internal/supervisord"
	"gitlab.com/Alvoras/balcon-exporter/internal/systemctl"
)

func Ok(ctx *fasthttp.RequestCtx, _ fasthttprouter.Params) {
	ctx.SetStatusCode(fasthttp.StatusOK)
}

func Status(ctx *fasthttp.RequestCtx, _ fasthttprouter.Params) {
	data := map[string]int{}

	if supervisord.Exporter != nil {
		for k, v := range supervisord.Exporter.GetAllStatusCode() {
			data[k] = v
		}
	}

	if systemctl.Exporter != nil {
		for k, v := range systemctl.Exporter.GetAllStatusCode() {
			data[k] = v
		}
	}

	jsonData, err := json.Marshal(data)
	if err != nil {
		ctx.SetStatusCode(fasthttp.StatusInternalServerError)
		return
	}

	fmt.Fprintf(ctx, string(jsonData))
}

func StatusByName(ctx *fasthttp.RequestCtx, params fasthttprouter.Params) {
	name := params.ByName("name")
	data := map[string]int{}

	if supervisord.Exporter != nil {
		for k, v := range supervisord.Exporter.GetAllStatusCode() {
			data[k] = v
		}
	}

	if systemctl.Exporter != nil {
		for k, v := range systemctl.Exporter.GetAllStatusCode() {
			data[k] = v
		}
	}

	if _, ok := data[name]; ok {
		fmt.Fprintf(ctx, strconv.Itoa(data[name]))
		return
	}

	ctx.SetStatusCode(fasthttp.StatusNotFound)
}
