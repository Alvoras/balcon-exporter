package main

import (
	"github.com/valyala/fasthttp"
	"gitlab.com/Alvoras/balcon-exporter/internal/config"
	"gitlab.com/Alvoras/balcon-exporter/internal/router"
	"gitlab.com/Alvoras/balcon-exporter/internal/supervisord"
	"gitlab.com/Alvoras/balcon-exporter/internal/systemctl"
)

func init() {
	config.Cfg.Load()
}

func main() {
	if config.Cfg.Exporters.Systemctl != nil {
		systemctl.Exporter = config.Cfg.Exporters.Systemctl.NewStateExporter(
			systemctl.GetStatusMap(),
			systemctl.GetStatuses,
			config.Cfg.Exporters.Systemctl,
		)
	}

	if config.Cfg.Exporters.Supervisord != nil {
		supervisord.Exporter = config.Cfg.Exporters.Supervisord.NewStateExporter(
			supervisord.GetStatusMap(),
			supervisord.GetStatuses,
			config.Cfg.Exporters.Supervisord,
		)
	}

	// supervisord.ServicesList.RegisterAll(config.Cfg.Exporters.Supervisord.Services)
	// systemctl.SystemctlExporter.RegisterAll(config.Cfg.Exporters.Systemctl.Services)

	go fasthttp.ListenAndServe(":"+config.Cfg.Port, router.R.Handler)

	if config.Cfg.Exporters.Supervisord != nil {
		go supervisord.Exporter.Poll()
	}

	if config.Cfg.Exporters.Systemctl != nil {
		go systemctl.Exporter.Poll()
	}

	select {}
}
