default:
	echo "No default make"

install:
	go build -o balcon-exporter
	mkdir /opt/balcon-exporter
	cp ./balcon-exporter /opt/balcon-exporter/

startup:
	sudo ln -rs "balcon-exporter.service /etc/systemd/system/balcon-exporter.service"
	sudo systemctl enable balcon-exporter
